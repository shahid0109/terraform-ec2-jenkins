terraform {
  backend "s3" {
    bucket = "gitlab-cicd-terraform-16-05-2024" # Replace with your actual S3 bucket name
    key    = "Gitlab/terraform.tfstate"
    region = "us-east-1"
  }
}
